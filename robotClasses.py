import RPi.GPIO as GPIO
import time
import random

GPIO.setmode(GPIO.BOARD)

TRIG = 12
ECHO = 16

TravelSpeed = 40 #Centimeters/second

GPIO.setup(TRIG,GPIO.OUT)

GPIO.setup(ECHO,GPIO.IN)

GPIO.setup(7,GPIO.OUT) #Right Backwards
GPIO.setup(11,GPIO.OUT) #Right Forwards
GPIO.setup(13,GPIO.OUT) #Left Backwards
GPIO.setup(15,GPIO.OUT) #Left Forwards

GPIO.setup(32,GPIO.OUT) #Servo

GPIO.setup(36,GPIO.OUT) #Left Turn Signal
GPIO.setup(37,GPIO.OUT) #Right Turn Signal

#Pulse Width Modulation(PWM)

rightB = GPIO.PWM(7,50)
rightF = GPIO.PWM(11,50)
leftB = GPIO.PWM(13,50)
leftF = GPIO.PWM(15,50)

servo = GPIO.PWM(32,50)

Servo = Servo()
Lights = Lights()
Robot = Move()

class Distance(object):
    def __init__(self):
        self.distance = 0

    def frontalDistance(self):
        #Turns Servo Forward
        Servo.straight()
        #Get Distance To Obstacle
        self.frontDistance = getMeasurment()
        #If it is a bad reading it will recaculate
        while self.frontDistance < 1:
            self.frontDistance = getMeasurment()

    def leftDistance(self)
        #Turns Servo Left
        Servo.left()
        #Get Distance To Obstacle
        self.leftDistance = getMeasurment()
        #If it is a bad reading it will recaculate
        while self.leftDistance < 1:
            self.leftDistance = getMeasurment()

    def rightDistance(self)
        #Turns Servo Right
        Servo.right()
        #Get Distance To Obstacle
        self.rightDistance = getMeasurment()
        #If it is a bad reading it will recaculate
        while self.rightDistance < 1:
            self.rightDistance = getMeasurment()

    def findWayToMove(self):
        if self.leftDistance > self.frontDistance:
            if self.leftDistance > self.rightDistance:
                self.way = "Left"
            elif:
                self.way = "Right"
        else if self.rightDistance > self.frontDistance:
            self.way = "Right"
        elif:
            self.way = "Front"

    def findWayToTurn(self):
        self.leftDistance = self.leftDistance()
        self.rightDistance = self.rightDistance()
        if self.leftDistance >= self.rightDistance:
            Robot.left(90)

        
            
class Move(object):
    def __init__(self):
        print("Robot Created")
            
    def forward(self, time):
        GPIO.output(11,True)
        GPIO.output(15,True)
        time.sleep(time)
        GPIO.output(11,False)
        GPIO.output(15,False)

    def reverse(self, time):
        Lights.hazards()
        time.sleep(1)

        rightB.start(50)
        leftB.start(50)
        time.sleep(time)
        rightB.stop()
        leftB.stop()
    
        time.sleep(1)
        Lights.off()
        

    def left(self, deg):
        time = deg / 180
        GPIO.output(15,True)
        time.sleep(time)
        GPIO.output(15,False)
    
    def right(self, deg):
        time = deg / 180
        GPIO.output(11,True)
        time.sleep(time)
        GPIO.output(11,False)

class Servo(object):
    def __init__(self):
        print("Servo Created")

    def straight(self):
        servo.start(7.5)
        time.sleep(1)
        servo.stop()

    def left(self):
        servo.start(12.5)
        time.sleep(1)
        servo.stop()

    def right(self):
        print ("Right")
        servo.start(2.5)
        time.sleep(1)
        servo.stop()

class Lights(object):
    def __init__(self):
        print("Lights Created")

    def hazards(self):
        GPIO.output(36,True)
        GPIO.output(37,True)

    def left(self):
        GPIO.output(36,True)

    def right(self):
        GPIO.output(37,True)

    def off(self):
        GPIO.output(36,False)
        GPIO.output(37,False)
