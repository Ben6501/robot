import RPi.GPIO as GPIO
import time
import random

GPIO.setmode(GPIO.BOARD)

TRIG = 12
ECHO = 16

TravelSpeed = 40 #Centimeters/second

GPIO.setup(TRIG,GPIO.OUT)

GPIO.setup(ECHO,GPIO.IN)

GPIO.setup(7,GPIO.OUT) #Right Backwards
GPIO.setup(11,GPIO.OUT) #Right Forwards
GPIO.setup(13,GPIO.OUT) #Left Backwards
GPIO.setup(15,GPIO.OUT) #Left Forwards

GPIO.setup(32,GPIO.OUT) #Servo

GPIO.setup(36,GPIO.OUT) #Left Turn Signal
GPIO.setup(37,GPIO.OUT) #Right Turn Signal

#Pulse Width Modulation(PWM)

rightB = GPIO.PWM(7,50)
rightF = GPIO.PWM(11,50)
leftB = GPIO.PWM(13,50)
leftF = GPIO.PWM(15,50)

servo = GPIO.PWM(32,50)

def getMeasurment():
    GPIO.output(TRIG,0)

    time.sleep(0.1)

    GPIO.output(TRIG,1)
    time.sleep(0.00001)
    GPIO.output(TRIG,0)

    while GPIO.input(ECHO) == 0:
        pass
    start = time.time()

    while GPIO.input(ECHO) == 1:
        pass
    stop = time.time()

    #Gets distance in centimeters
    
    distance = (stop - start) * 17000
    print (distance)
    return distance

def findDistance():
    distance = getMeasurment()
    if distance < 1:
        findDistance()
    else:
        return distance

def getTimeToMove(distance):
    distance = distance - 10 #Subtracts 10 so it has room to turn
    print(distance)
    secondsCanTravel = distance / TravelSpeed # Finds how long it can go forward
    print(secondsCanTravel)
    move(secondsCanTravel)
    getWayToTurn()
    getMeasurment()

def rightTurnSignal():
    #Will blink 3 times
    for x in range(0,3):
        GPIO.output(37,True)
        time.sleep(0.5)
        GPIO.output(37,False)
        time.sleep(0.5)
    #Turns after signal
    turnRight()

def turnRight():
    GPIO.output(15,True)
    time.sleep(0.5)
    GPIO.output(15,False)

def leftTurnSignal():
    #Will blink 3 times
    for x in range(0,3):
        GPIO.output(36,True)
        time.sleep(0.5)
        GPIO.output(36,False)
        time.sleep(0.5)
    #Turns after singal
    turnLeft()

def turnLeft():
   GPIO.output(11,True)
   time.sleep(0.5)
   GPIO.output(11,False)

def getWayToTurn():
    #Moves Servo Right and gets distance to obstacle
    servo.start(2.5)
    time.sleep(1)
    right = findDistance()
    
    #Moves Servo Left and gets distance to obstacle
    servo.ChangeDutyCycle(12.5)
    time.sleep(1)
    left = findDistance()

    #Returns Servo to neutral

    servo.ChangeDutyCycle(7.5)
    time.sleep(1)
    servo.stop()

    #Finds which way has more room to move

    if left > right:
        leftTurnSignal()
    else:
        rightTurnSignal()

def move(Travel):
    GPIO.output(11,True)
    GPIO.output(15,True)
    #Waits the time it can Travel before turning off the motors
    time.sleep(Travel)
    GPIO.output(11,False)
    GPIO.output(15,False)

def reverse():
    #Turns on Hazards
    GPIO.output(36,True) 
    GPIO.output(37,True)
    
    time.sleep(1)

    rightB.start(50)
    leftB.start(50)
    time.sleep(2)
    rightB.stop()
    leftB.stop()
    
    time.sleep(1)
    
    #Turns off Hazards
    GPIO.output(36,False)
    GPIO.output(37,False)

    getMeasurment()

def stop():
    GPIO.cleanup()

def findWhichWayToMove():
    distance = findDistance()

    #Finds which way to move
    if distance > 15:
        #Forward
        getTimeToMove(distance)
    elif distance > 10:
        #Turns
        getWayToTurn()
    elif distance < 10:
        #Too close to wall
        reverse()

findWhichWayToMove()

stop()
